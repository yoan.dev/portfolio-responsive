import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AOS from 'aos';
import 'aos/dist/aos.css';
import "animate.css/animate.min.css";
import App from './Components/App';
import * as serviceWorker from './serviceWorker';

AOS.init({
    easing: 'ease-in-out-back',
});

if (module.hot) {
    module.hot.accept();
    }

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
