import React, { Component } from 'react';

import ImageLogo from './ImageLogo';
import Foot from './Foot';
import TextAbout from './TextAbout';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import imgBac from '../../images/diploma.png';
import imgCharette from '../../images/charette.png';
import imgPoker from '../../images/cards.png';
import imgCook from '../../images/cooking.png';
import imgBook from '../../images/books.png';
import imgDice from '../../images/dice.png';
import imgOpen from '../../images/openClassrooms.png';

import {styles} from './styles';

class Presentation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: [0,0,0,0,0,0,0],
      isNeverHover: true,
    };
  }
  _onMouseEnter = event => {
    let imgAlt = event.currentTarget.firstChild.firstChild.alt;
    if (imgAlt === 'imgBac') {
      this.setState({hover: [1,0,0,0,0,0,0]});
    } else if (imgAlt === 'imgCharette') {
      this.setState({hover: [0,1,0,0,0,0,0]});
    } else if (imgAlt === 'imgPoker') {
      this.setState({hover: [0,0,1,0,0,0,0]});
    } else if (imgAlt === 'imgCook') {
      this.setState({hover: [0,0,0,1,0,0,0]});
    } else if (imgAlt === 'imgBook') {
      this.setState({hover: [0,0,0,0,1,0,0]});
    } else if (imgAlt === 'imgDice') {
      this.setState({hover: [0,0,0,0,0,1,0]});
    } else if (imgAlt === 'imgOpen') {
      this.setState({hover: [0,0,0,0,0,0,1]});
    }
    this.setState({isNeverHover: false});
  }
  _onMouseLeave = () => {
      this.setState({hover: [0,0,0,0,0,0,0]});
  }
  render() {
    const {hover, isNeverHover} = this.state;
    const {width, height} = this.props;
    const widthBlocImg = Math.floor(width/6)+'px';
    const heightImg = width > height ? Math.floor(height * 0.12)+'px' : Math.floor(height * 0.08)+'px';
    const heightShadow = width > height ? Math.floor(height * 0.03)+'px' : Math.floor(height * 0.02)+'px';
    const heightFoot = Math.floor(height * 0.054)+'px';
    const [classNameShadow, classNameImg, classNames] = [[],[],[]];
    const smallImg = width < height ? true : false;
    for (let i = 0; i < 7; i++) {
      classNameShadow[i] = hover.indexOf(1) === i ? ('object')+(i+1).toString()+'__shadow--anim' : ('object'+(i+1).toString()+'__shadow');
      classNames[i] = hover.indexOf(1) === i ? ('object')+(i+1).toString()+'--anim' : ('object'+(i+1).toString());
      classNameImg[i] = hover.indexOf(1) === i ? ('object')+(i+1).toString()+'__img--anim' : ('object'+(i+1).toString()+'__img');
   }
    return (
    <Row style={styles.presentation} className="h-100 " id="présentation">
      <Col xs={12} className=" d-flex flex-column justify-content-center mt-5">
        {width < height && (
          <Row className="d-flex justify-content-center">
            <Col xs={10}>
              {hover.indexOf(1) !== -1 && <TextAbout name={dataTextAbout[hover.indexOf(1)]} width={width} height={height} />}
            </Col>
          </Row>
        )}
        <Row className=""> {/* LIGNE 1 */}
          <Col xs={1} className="">
            
          </Col>
          <Col xs={2} className="p-0 d-flex align-items-center">
            <ImageLogo height={height} width={width} widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[0]}
              src={imgBac} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgBac" name='bac' isNeverHover={isNeverHover}
              className={classNameImg[0]} classNameShadow={classNameShadow[0]} heightShadow={heightShadow} smallImg={smallImg}
              />
          </Col>
          <Col xs={2}  style={hover.indexOf(1) === 0 ? {marginTop: '-'+height/20+'px'} : {marginTop: '-'+height/10+'px'}}>
            {width > height && (
              ((hover.indexOf(1) === 0) || (hover.indexOf(1) === 1)) && (
                <TextAbout name={dataTextAbout[hover.indexOf(1)]} width={width} height={height} />
              )
            )}
          </Col>
          <Col xs={2} className=" p-0 d-flex align-items-center">
            <ImageLogo height={height} width={width} widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[1]}
              src={imgBook} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgCharette" name='charette'
              className={classNameImg[1]} classNameShadow={classNameShadow[1]} heightShadow={heightShadow} smallImg={smallImg}/>
          </Col>
          {width < height ? (
            <Col xs={1} className="position-relative" style={{left: Math.floor(height/30)+'px', top: Math.floor(height/20)+'px'}}>
              <Foot isRight={true} isVertical={false} heightFoot={heightFoot} />
          </Col>
          ) : (
          <Col xs={1} className="position-relative" style={{left: '-'+Math.floor(height/30)+'px'}}>
              <Foot isRight={false} isVertical={false} heightFoot={heightFoot} />
              <div style={{position: 'absolute', bottom: '0px', left: Math.floor(width/11.5)+'px'}}>
                <Foot isRight={true} isVertical={false} heightFoot={heightFoot} />
                </div>
              <div style={{position: 'absolute',bottom: Math.floor(height/22)+'px', left: Math.floor(width/8)+'px'}}>
                <Foot isRight={false} isVertical={false} heightFoot={heightFoot} />
                </div>
          </Col>
          )}
          
          <Col xs={1} className="">
          </Col>
          <Col xs={2} className="p-0 d-flex align-items-center">
            <ImageLogo height={height} width={width} widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[2]}
              src={imgDice} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgPoker" name="poker"
              className={classNameImg[2]} classNameShadow={classNameShadow[2]} heightShadow={heightShadow} smallImg={smallImg}/>
          </Col>
          <Col xs={1} className="">

          </Col>
        </Row>
        <Row className="">{/* LIGNE 2 */}
          <Col xs={1} className="">
          
          </Col>
          <Col xs={2} >
            <Foot isRight={true} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={2} className="">

          </Col>
          <Col xs={2} className="position-relative" style={{top: Math.floor(height/32)+'px'}}>
            <Foot isRight={false} isVertical={true} isReverse={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={2}  style={{marginTop: '-'+height/4+'px'}}>
            {width > height && (
              hover.indexOf(1) === 2 && <TextAbout name='dice' width={width} height={height} />
            )}
          </Col>
          <Col xs={2} className="">
            <Foot isRight={true} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={1} className=""></Col>
        </Row>
        <Row className="">{/* LIGNE 3 */}
          <Col xs={1} className=""></Col>
          <Col xs={2} className="position-relative" style={{top: Math.floor(height/50)+'px'}}>
            <Foot isRight={false} isVertical={true} heightFoot={heightFoot}/>
          </Col>
          <Col xs={2} className=""></Col>
          <Col xs={2} className="">
            
          </Col>
          <Col xs={2} className=""></Col>
          <Col xs={2}  className="position-relative" style={{top: Math.floor(height/50)+'px'}}>
            <Foot isRight={false} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={1} className=""></Col>
        </Row>
        <Row className="">{/* LIGNE 4 */}
          <Col xs={1} className=""></Col>
          <Col xs={2} className="">
            <Foot isRight={true} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={2} style={{marginTop: '-'+height/7+'px'}}>
            {width > height && (
              hover.indexOf(1) === 3 && <TextAbout name="cook" width={width} height={height} />
            )}
          </Col>
          <Col xs={2} className="p-0 d-flex align-items-center">
          <ImageLogo height={height} width={width} widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[3]}
            src={imgCook} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgCook" name="cook"
            className={classNameImg[3]} classNameShadow={classNameShadow[3]} heightShadow={heightShadow} smallImg={smallImg}/>
          </Col>
          <Col xs={2} className=""></Col>
          <Col xs={2} className="">
            <Foot isRight={true} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={1} className=""></Col>
        </Row>
        <Row className="">{/* LIGNE 5 */}
          <Col xs={1} className=""></Col>
          <Col xs={2} className="position-relative" style={{bottom: Math.floor(height/40)+'px'}}>
            <Foot isRight={false} isVertical={true} isMarginB={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={2} className=""></Col>
          <Col xs={2} className="position-relative" style={{top: Math.floor(height/32)+'px'}}>
            <Foot isRight={true} isVertical={true} isReverse={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={2} className=""></Col>
          <Col xs={2} className="position-relative" style={{bottom: Math.floor(height/40)+'px'}}>
            <Foot isRight={false} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={1} className=""></Col>
        </Row>
        <Row className="">{/* LIGNE 6 */}
          <Col xs={1} className=""></Col>
          <Col xs={2} className="">
            <Foot isRight={true} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={2} style={{marginTop: '-'+height/8+'px'}}>
            {width > height && (
              hover.indexOf(1) === 4 && <TextAbout name='charette' width={width} height={height} />
            )}
          </Col>
          <Col xs={2} className="">
            
          </Col>
          <Col xs={2} className=""></Col>
          <Col xs={2} className="">
           <Foot isRight={true} isVertical={true} heightFoot={heightFoot} />
          </Col>
          <Col xs={1} className=""></Col>
        </Row>
        <Row className="">{/* LIGNE 7 */}
          <Col xs={1} className=""></Col>
          <Col xs={2} className="p-0 d-flex align-items-center">
            <ImageLogo height={height} width={width}widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[4]}
              src={imgCharette} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgBook" name="book"
              className={classNameImg[4]} classNameShadow={classNameShadow[4]} heightShadow={heightShadow} smallImg={smallImg}/>
          </Col>
          {width < height ? (
            <Col xs={1} className="position-relative" style={{left: Math.floor(height/30)+'px', top: Math.floor(height/20)+'px'}}>
              <Foot isRight={true} isVertical={false} heightFoot={heightFoot} />
          </Col>
          ) : (
          <Col xs={1} className="position-relative" style={{left: '-'+Math.floor(height/30)+'px'}}>
              <Foot isRight={false} isVertical={false} heightFoot={heightFoot} />
              <div style={{position: 'absolute', bottom: '0px', left: Math.floor(width/11.5)+'px'}}>
                <Foot isRight={true} isVertical={false} heightFoot={heightFoot} />
                </div>
              <div style={{position: 'absolute',bottom: Math.floor(height/22)+'px', left: Math.floor(width/8)+'px'}}>
                <Foot isRight={false} isVertical={false} heightFoot={heightFoot} />
                </div>
          </Col>
          )}
          <Col xs={1} className="">
            
          </Col>
          <Col xs={2} className=" p-0 d-flex align-items-center">
            <ImageLogo height={height} width={width} widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[5]}
            src={imgPoker} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgDice" name="dice"
            className={classNameImg[5]} classNameShadow={classNameShadow[5]} heightShadow={heightShadow} smallImg={smallImg}/>
          </Col>
          <Col xs={2} style={{marginTop: '-'+height/3+'px'}}>
          {width > height && (
              ((hover.indexOf(1) === 5) || (hover.indexOf(1) === 6)) && (
                <TextAbout name={dataTextAbout[hover.indexOf(1)]} width={width} height={height} />
              )
            )}
          </Col>
          <Col xs={2} className="p-0 d-flex align-items-center">
            <ImageLogo height={height} width={width} widthBlocImg={widthBlocImg} heightImg={heightImg} valueAnim={hover[6]}
              src={imgOpen} onMouseEnter={this._onMouseEnter} onMouseLeave={this._onMouseLeave} alt="imgOpen" name="open"
              className={classNameImg[6]} classNameShadow={classNameShadow[6]} heightShadow={heightShadow} smallImg={smallImg}/>
          </Col>
          <Col xs={1} className=""></Col>
        </Row>
      </Col>
    </Row>
  );
  }
}

const dataTextAbout = [
  'bac',
  'book',
  'dice',
  'cook',
  'charette',
  'poker',
  'open'
];
export default Presentation;