const presentation = {
    background: 'radial-gradient(rgb(141, 184, 161), #04454D)',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
};

export const styles = {
    presentation,
};