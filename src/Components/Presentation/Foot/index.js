import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';

import imgFootRight from '../../../images/footRight.png';


class Foot extends Component {
  render() {
    const {isRight, isVertical, isReverse, heightFoot} = this.props;
    const rotate = isVertical ? (isReverse ? 'rotate(0deg)' : 'rotate(180deg)') : 'rotate(90deg)';

    return (
    <Row className="h-100" >
        <Col className="d-flex justify-content-center align-items-center">
            {isRight ? (
                <Image src={imgFootRight} style={{transform: rotate, height: heightFoot}} className={isReverse ? 'ml-3 ml-lg-4' : 'mr-3 mr-lg-4'}  />
            ) : (
                <Image src={imgFootRight} style={{transform: rotate +'scaleX(-1)',height: heightFoot}} className={isReverse ? 'mr-3 mr-lg-4' : 'ml-3 ml-lg-4'}  />
            )}
        </Col>
    </Row>
  );
  }
}

export default Foot;