const textAbout =  {
  backgroundColor: '#12152B',
  border: 'solid 1px rgb(143, 245, 228)',
  borderRadius: '15px',
  zIndex: 2,
};
const textAboutPhone = (height, width) => {
  return {
    width: Math.round(0.8*width)+'px',
    backgroundColor: '#12152B',
    border: 'solid 1px rgb(143, 245, 228)',
    borderRadius: '15px',
    zIndex: 2,
    margin: 'auto',
    marginTop: '-'+Math.round(height/4.7)+'px',
    
  }
  
};

const textAboutTitle = textSize => {
    return {
      color: 'rgb(202, 224, 213)',
      fontSize: textSize+'px',
      textAlign: 'center',
      fontWeight: 'bold',

    }
};

const textAboutSummarise = textSize => {
  return {
    color: 'rgb(202, 224, 213)',
    fontSize: textSize+'px',
    textAlign: 'center',
    marginBottom: '5px',
    fontWeight: 'bold',
  }
};
const textAboutPoint = textSize => {
  return {
    color: 'rgb(202, 224, 213)',
    textAlign: 'left',
    fontSize: textSize+'px',
    marginBottom: '5px',
    fontWeight: 'bold',
  }
};

const point = size => {
  return {
    width: size+'px',
  height: size+'px',
  borderRadius: '50%',
  marginTop: Math.floor(size/1.2)+'px',
  marginLeft: Math.floor(size)+'px',
  backgroundColor: 'rgb(133, 224, 209)',
  }
  
};

export const styles = {
  textAbout,
  textAboutSummarise,
  textAboutTitle,
  textAboutPhone,
  point,
  textAboutPoint,
};