import React, { Component } from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import {styles} from './styles';

class TextAbout extends Component {
  render() {
    const {name, height, width} = this.props;
    let textSizeTitle, textSizeSummarise, pointSize;
    const textAboutStyle = width < height ? styles.textAboutPhone(height, width) : styles.textAbout;
    if (width > height && width >= 992) {
        textSizeTitle = 16;
        textSizeSummarise = 16;
        pointSize = 10;
    } else if (width >= 768) {
        textSizeTitle = 14;
        textSizeSummarise = 12;
        pointSize = 8;
    } else if(width >= 576) {
        textSizeTitle = 12;
        textSizeSummarise = 10;
        pointSize = 8;
    } else if (width < 576) {
        textSizeTitle = 12;
        textSizeSummarise = 10;
        pointSize = 6;
    }
    return (
    <Row style={textAboutStyle} className="d-flex flex-column position-absolute" 
    data-aos="flip-left" data-aos-duration="700" >
      <Col xs={12} style={styles.textAboutTitle(textSizeTitle)} >{data[name].title}</Col>
      <Col xs={12} style={styles.textAboutSummarise(textSizeSummarise)} >{data[name].summarise}</Col>
      <Row className={width > height ? "d-flex flex-column" : "d-flex flex-row flex-wrap"}>
        {data[name].skills.map((point,index) => (
          <Col xs={width > height ? 12 : 6} key={point}>
            <Row className="d-flex justify-content-end ">
              <Col xs={0} style={styles.point(pointSize)}></Col>
              <Col xs={10} style={styles.textAboutPoint(textSizeSummarise)} key={point}>{data[name].skills[index]}</Col>
            </Row>
          </Col>
          
        ))}
      </Row>
    </Row>
  );
  }
};

const data = {
  'bac': {
    title: 'BAC',
    summarise: '2012 - Bac science de l\'ingénieur avec option mathématiques.',
    skills: [],
  },
  'charette': {
    title: 'Saisonnier',
    summarise: '2013 à 2016 - Vente ambulante sur les plages méditerranéennes.',
    skills: ['Motivation', 'Esprit d\'équipe', 'Détermination'],
  },
  'poker': {
    title: 'Poker',
    summarise: '2015 et 2016 - Étude et analyse du Jeu. Élaboration d\'un programme d\'aide à la décision au poker.',
    skills: ['Créativité', 'Détermination', 'Passion'],
  },
  'cook': {
    title: 'Cuisine',
    summarise: '2018 - Commis de cuisine une année.',
    skills: ['Rigueur', 'Dicispline', 'Esprit d\'équipe'],
  },
  'book': {
    title: 'Apprentissage',
    summarise: '2019 - Étude en autonomie sur différentes technologies, JS, CSS et approfondissement du framework React et anglais technique.',
    skills: ['Autodidacte','Passion','Organisation', 'Discipline'],
  },
  'dice': {
    title: 'PICO',
    summarise: '2019 - Création d\'un projet de jeu de dès, élaboration d\'un cahier des charges et d\'une application avec React & Redux.',
    skills: ['Détermination','Motivation', 'Créativité', 'Passion'],
  },
  'open': {
    title: 'Formation',
    summarise: '2019 à 2021 - Formation avec l\'école OpenClassrooms dans le but d\'obtenir un diplôme BAC+3/4 de Développeur d\'application.',
    skills: ['Détermination', 'Engagement', 'Proactivité', 'Rigueur'],
  },
}

export default TextAbout;