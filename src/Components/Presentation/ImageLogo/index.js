import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './ImageLogo.css';

class ImageLogo extends Component {
  render() {
      const {height, width, widthBlocImg, heightImg, src, onMouseEnter, onMouseLeave, alt, className, classNameShadow, heightShadow,smallImg, isNeverHover} = this.props;
        const classNameNeverHover = smallImg ? (isNeverHover ? 'object1__NeverHover--small' : className+'--small') : (isNeverHover ? 'object1__neverHover' : className);
    return (
    <Row className="m-0 w-100 d-flex justify-content-center" >
        <Col xs={6} 
            className="d-flex justify-content-center p-0" 
            style={{height:heightImg, width: widthBlocImg}}
            onMouseEnter={event => onMouseEnter(event)}
            onMouseLeave={() => onMouseLeave()}
            >
              <div className={classNameNeverHover}><img  src={src} alt={alt} style={{height: heightImg}} /></div>
            
        </Col>
        {width > height ? (
          <Col xs={7} style={{borderRadius:'50%', backgroundColor: 'black', height: heightShadow}} className={classNameShadow}></Col>
        ) : (
          <Col xs={9} style={{borderRadius:'50%', backgroundColor: 'black', height: heightShadow}} className={classNameShadow}></Col>
        )}
    </Row>
  );
  }
}

export default ImageLogo;