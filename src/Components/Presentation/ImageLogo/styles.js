import styled, {keyframes} from 'styled-components';

const shadow = shadowHeight => {
    return {
        height: (shadowHeight*0.03)+'px',
        backgroundColor: 'rgb(15, 15, 15)',
        borderRadius: '50%',
    }
};

const image = imageHeight => {
    const logoFrame = keyframes`
        0% {
            transform: scale(1) translateY(0px);
        }
        100% {
            transform: scale(1.2) translateY(-40px);
        }`;
        const imageAnim = styled.div`
            height: ${imageHeight};
            animation: ${logoFrame} 0.3s forwards;
            `;

    return imageAnim;
};
const imageReverse = imageHeight => {

    const logoFrameReverse = keyframes`
        0% {
            transform: scale(1.2) translateY(-40px);
        }
        100% {
            transform: scale(1) translateY(0px);
        }`;
        const imageAnim = styled.div`
            height: ${imageHeight};
            animation: ${logoFrameReverse} 0.3s forwards;
            `;

    return imageAnim;
};



export const styles = {
    shadow,
    image,
    imageReverse,
};