import imgGeo from '../../images/geometry.jpg'

const landpage = {
  background: `url(${imgGeo}) fixed no-repeat`,
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};

const title1 = fontSize => {
  return {
    fontSize: fontSize+'px',
    fontWeight: '400',
    fontFamily: 'Audiowide, cursive',
    fontVariant: 'small-caps',
    textAlign: 'center',
    color: 'rgb(202, 224, 213)',
  };
};

const summarize = (fontSize) => {
  return {
    border: 'solid 1px rgba(172, 193, 182, 0.7)',
    borderRadius: '10px',
    fontSize: fontSize+'px',
    fontWeight: '700',
    color: 'rgb(202, 224, 213)',
    backgroundColor: '#12152B',
  };
};

const image = height => {
  return {
    height: height+'px',
    width: height+'px',
    maxHeight: '370px',
    maxWidth: '370px',
    minHeight: '150px',
    minWidth: '150px',
    borderRadius:'50%',
    border: 'solid 2px #ACC1B6',
    userSelect: 'none',
  }
}

export const styles = {
    landpage,
    title1,
    summarize,
    image,
}