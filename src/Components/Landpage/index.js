import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import imgProfil from '../../images/yoan.jpg';

import './LandPage.css';
import {styles} from './styles';

class Landpage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: null,
      classNames: 'landpage__display__none'
    };
  }
  _onDisplayText = () => {
    this.setState({classNames: 'landpage__display__yes'});
  }
  componentDidMount = () => {
    const timer = setTimeout(this.timeToDisplay, 1300);
    this.setState({timer})
  }
  timeToDisplay = () => {
    this.setState({classNames: 'landpage__display__yes'});
  }
  compo
  render() {
    const {classNames} = this.state;
    const {width, height} = this.props;
    let fontSize, fontSizeResume;
    if (width > height) {
      if (width <= 576) {fontSize = 30; fontSizeResume = 10;}
      if (width > 576 && width <= 768) {fontSize = 40; fontSizeResume = 12}
      if (width > 768 && width <= 992) {
        if (height <= 500) {fontSize = 45; fontSizeResume = 12}
        if (height > 500 && height <= 650) {fontSize = 52; fontSizeResume = 16}
        if (height > 650) {fontSize = 60;fontSizeResume = 16}
      }
      if (width > 992) {fontSize = 80; fontSizeResume = 18}
    } else {
      if (width <= 400) {fontSize = 35; fontSizeResume = 12}
      if (width > 400 && width <= 576) {fontSize = 50; fontSizeResume = 14}
      if (width > 576 && width <= 768) {fontSize = 55; fontSizeResume = 16}
      if (width > 768 && width <= 992) {fontSize = 65; fontSizeResume = 18}
      if (width > 992) {fontSize = 80; fontSizeResume = 18}
    }
    return (
          <Row style={styles.landpage} className="h-100" id="accueil" >
          {width > height ? (
          <Col xs={12} >
            <Row className="d-flex align-items-center align-items-center" style={{height: Math.round(height*0.3)+'px'}}>
              <Col xs={12} style={styles.title1(fontSize)} className="d-flex justify-content-center">
                Développeur d'application
              </Col>
            </Row>
            <Row style={{height: Math.round(height*0.4)+'px'}} className="d-flex justify-content-around">
              <Col xs={4} className="px-2 px-sm-3 px-lg-4 px-xl-5 d-flex align-items-center">
                <Col xs={12} style={styles.summarize(fontSizeResume)} className={classNames}>
                  En formation avec l'école OpenClassrooms, je recherche une entreprise pour m'accueillir en alternance sur 24 mois.</Col>
                </Col>
              <Col xs={3} className="d-flex justify-content-center align-items-center p-0">
                <img src={imgProfil} alt="imgProfil" style={styles.image(width/4.5)} />
              </Col>
              <Col xs={4} className="px-2 px-sm-3 px-lg-4 px-xl-5 d-flex align-items-center position-relative" >
                <Col style={styles.summarize(fontSizeResume)} className={classNames} >
                  Passionné du digital, j'apprends en autonomie les différentes technologies pour créer des applications et logiciels solides.
                </Col>
              </Col>
            </Row>
            <Row className="d-flex justify-content-center"  style={{height: Math.round(height*0.3)+'px'}}>
              <Col xs={12} style={styles.title1(fontSize)} className="d-flex justify-content-center align-items-center">
                Bousquet Yoan
              </Col>
            </Row>
          </Col>) : (
          <Col xs={12} >
          <Row className="d-flex align-items-center align-items-center mt-3" style={{height: Math.round(height*0.2)+'px'}}>
            <Col xs={12} style={styles.title1(fontSize)} className="d-flex justify-content-center">
              Développeur d'application
            </Col>
          </Row>
          <Row style={{height: Math.round(height*0.6)+'px'}} className="d-flex justify-content-center">
            <Col xs={8} className="d-flex align-items-center">
              <Col style={styles.summarize(fontSizeResume)} className={classNames}>
                En formation avec l'école OpenClassrooms, je recherche une entreprise pour m'accueillir en alternance sur 24 mois.</Col>
              </Col>
            <Col xs={12} className="d-flex justify-content-center align-items-center p-0">
              <img src={imgProfil} alt="imgProfil" style={styles.image(width/4)} />
            </Col>
            <Col xs={8} className="d-flex align-items-center">
              <Col style={styles.summarize(fontSizeResume)} className={classNames}>
              Passionné du digital, j'apprends en autonomie les différentes technologies pour créer des applications et logiciels solides.
              </Col>
            </Col>
          </Row>
          <Row className="d-flex justify-content-center"  style={{height: Math.round(height*0.2)+'px', marginTop: '-5px'}}>
            <Col xs={12} style={styles.title1(fontSize)} className="d-flex justify-content-center align-items-center">
              Bousquet Yoan
            </Col>
          </Row>
        </Col>
          )}
        </Row>
      
  );
  }
}

export default Landpage;