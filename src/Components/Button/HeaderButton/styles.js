const button = {
    backgroundColor: 'rgb(202, 224, 213)',
    borderRadius: '4px',
    userSelect: 'none',
};
const buttonHover = {
    backgroundColor: '#12152B',
    borderRadius: '4px',
    textAlign: 'center',
    transition: 'transform 0.2s',
    transform: 'translate(3px, -4px)',
}
const buttonContain = {
    backgroundColor: '#12152B',
    borderRadius: '2px',
    textAlign: 'center',
    transition: 'transform 0.2s',
    borderBottom: 'solid 1px #12152B',
};
const buttonText = fontSize => {
    return {
        color: 'rgb(202, 224, 213)',
        fontSize: fontSize+'px',
    }
    
}

export const styles = {
    button,
    buttonContain,
    buttonText,
    buttonHover,
};
