import React, {Component} from 'react';
import {Link} from 'react-scroll';
import {styles} from './styles';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class HeaderButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      styleBtn: styles.buttonContain,
    }
  }
  _onMouseEnter = () => {
    this.setState({styleBtn: styles.buttonHover});
  }
  _onMouseLeave = () => {
    this.setState({styleBtn: styles.buttonContain});
  }
  render() {
    const {styleBtn} = this.state;
    const {xs, fontSize, onClick, isSmallHeader} = this.props;
    const name = this.props.name.charAt(0).toUpperCase() + this.props.name.slice(1);
    const onClickBtn = isSmallHeader ? (() => onClick()) : function(){};
    let nameToLink = this.props.name;
    if (this.props.name === 'présen.') {
      nameToLink = 'présentation';
    } else if (this.props.name === 'compét.') {
      nameToLink = 'compétences'
    }
      return(
          <Col xs={xs} sm={2} xl={1} 
            className="mr-lg-2 ml-lg-2 mr-1 ml-1 my-1 my-sm-2" 
            style={styles.button}
            onMouseEnter={this._onMouseEnter}
            onMouseLeave={this._onMouseLeave}
          >
            <Link
              activeClass="active"
              to={nameToLink}
              onClick={onClickBtn}
              spy={true}
              smooth={true}
              offset={0}
              duration= {500}
            >
            <Row className="text-center button" style={styleBtn}>
              <Col className="h6 font-weight-bold pt-1" style={styles.buttonText(fontSize)}>{name}</Col>
            </Row>
          </Link>
          </Col>
      );
  }
}

export default HeaderButton;
