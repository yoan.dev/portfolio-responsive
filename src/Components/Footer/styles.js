const text = fontSize => {
  return {
    fontSize: fontSize+'px',
    color: 'rgb(202, 224, 213)',
    fontWeight: '700',
  }
}

export const styles = {
    text,
};