import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import imgGitlab from '../../images/gitlab.png';
import imgLinkedin from '../../images/linkedin.png';
import {styles} from './styles';

class Footer extends Component {
  render() {
    const {width, height} = this.props;
    let fontSize = 16, logoSize = 48;
    if (width <= 576) {fontSize = 12; logoSize = 32}
    if (width > 576 && width <= 992) {fontSize = 14; logoSize = 40}
    if (width > 992) {fontSize = 16; logoSize = 48}
    return (
    <Row style={{height: Math.round(height/6)+'px',minHeight: '80px', backgroundColor: '#12152B'}} className="d-flex justify-content-center">
      <Col xs={10} style={styles.text(fontSize)} className='d-flex justify-content-center align-items-center'>
      Bousquet Yoan © 2020 <a href="https://www.linkedin.com/in/yoan-bousquet-645162165"  target="_blank" rel="noopener noreferrer">
        <img src={imgLinkedin} alt="linkedin" style={{height: logoSize+'px', width: logoSize+'px'}} /></a>
      <a href="https://gitlab.com/yoan.dev"  target="_blank" rel="noopener noreferrer"><img src={imgGitlab} alt="gitlab" style={{height: logoSize+'px', width: logoSize+'px'}} /></a>
      </Col>
    </Row>
  );
  }
}

export default Footer;