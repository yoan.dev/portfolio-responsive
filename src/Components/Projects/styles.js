const projects = {
  background: 'radial-gradient(rgb(141, 184, 161), #04454D)',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};
const skillsTitle = (height, divise) => {
  return {
    position: 'absolute',
    color: 'rgb(202, 224, 213)',
    marginTop: (height/divise)+'px',
  };
};


export const styles = {
  projects,
  skillsTitle,
};