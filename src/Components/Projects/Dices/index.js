import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import dices1 from '../../../images/des1.png';
import dices2 from '../../../images/des2.png';
import dices3 from '../../../images/des3.png';
import dices4 from '../../../images/des4.png';
import dices5 from '../../../images/des5.png';
import dices6 from '../../../images/des6.png';
import imgRefresh from '../../../images/refresh.png';


import {styles} from './styles';

class Dices extends Component {
  render() {
    const {width, onClick, random, fontSize} = this.props;
    let diceHeight, diceMarginLeft, marginBottom;
    if (width <= 576) {diceHeight = 20; diceMarginLeft = 3;marginBottom = 1;}
    if (width > 576 && width <= 768) {diceHeight = 28; diceMarginLeft = 5;marginBottom = 1;}
    if (width > 768 && width <= 992) {diceHeight = 36; diceMarginLeft = 7;marginBottom = 0;}
    if (width > 992 && width <= 1200) {diceHeight = 44; diceMarginLeft = 9;marginBottom = 4;}
    if (width > 1200) {diceHeight = 50; diceMarginLeft = 10;marginBottom = 7;}
    return (
    <Row className="p-0 w-100 m-0">
      <Col xs={0}  className="d-flex justify-content-start" style={{padding: '0 15px'}}>
        <div style={styles.sentence(fontSize, width)}>Exemple trouver {' ' + dataDice[random].toFind}</div>
        
      </Col>
      <Col xs={6} className="p-0 d-flex justify-content-start" style={{marginTop: '-'+marginBottom+'px'}}>
        {dataDice[random].dices.map((dice, index) => {
          switch (dice) {
              case 1:
                  return <img src={dices1} alt='des1' className="dice" key={index} style={styles.dice(diceHeight, diceMarginLeft)} />
              case 2:
                  return <img src={dices2} alt='des2' className="dice" key={index} style={styles.dice(diceHeight, diceMarginLeft)} />
              case 3:
                  return <img src={dices3} alt='des3' className="dice" key={index} style={styles.dice(diceHeight, diceMarginLeft)} />
              case 4:
                  return <img src={dices4} alt='des4' className="dice" key={index} style={styles.dice(diceHeight, diceMarginLeft)} />
              case 5:
                  return <img src={dices5} alt='des5' className="dice" key={index} style={styles.dice(diceHeight, diceMarginLeft)} />
              case 6:
                  return <img src={dices6} alt='des6' className="dice" key={index} style={styles.dice(diceHeight, diceMarginLeft)} />
              default: break;    
          }
          return null;
            })}
          <img src={imgRefresh} alt="refresh" style={styles.refresh(diceHeight, diceMarginLeft)} onClick={() => onClick()} />
      </Col>
    </Row>
  );
  }
}

const dataDice = {
  1: {
    toFind: 39,
    dices: [4,5,3,1,5],
  },
  2: {
    toFind: 15,
    dices: [6,6,3,6,3],
  },
  3: {
    toFind: 73,
    dices: [6,1,4,5,3],
  },
  4: {
    toFind: 49,
    dices: [1,4,1,2,5],
  },
  5: {
    toFind: 42,
    dices: [4,3,5,6,5],
  },
  6: {
    toFind: 21,
    dices: [6,3,5,2,4],
  },
  7: {
    toFind: 24,
    dices: [4,3,4,6,4],
  },
  8: {
    toFind: 54,
    dices: [2,4,1,5,5],
  },
  9: {
    toFind: 43,
    dices: [2,3,3,2,5],
  },
  10: {
    toFind: 71,
    dices: [6,5,3,5,2],
  },
  11: {
    toFind: 77,
    dices: [3,4,5,5,6],
  },
  12: {
    toFind: 22,
    dices: [2,1,3,5,5],
  },
  13: {
    toFind: 56,
    dices: [3,6,5,5,2],
  },
  14: {
    toFind: 10,
    dices: [5,6,1,5,1],
  },
  15: {
    toFind: 46,
    dices: [1,4,2,6,6],
  },
  16: {
    toFind: 43,
    dices: [5,3,6,6,2],
  },
  17: {
    toFind: 26,
    dices: [2,6,1,1,2],
  },
  18: {
    toFind: 60,
    dices: [6,4,5,6,6],
  },
  19: {
    toFind: 6,
    dices: [4,3,6,5,4],
  },
  20: {
    toFind: 8,
    dices: [6,2,4,6,6],
  },
};

export default Dices;