const sentence = (fontSize, padding) => {
  return {
    width: 'auto',
    margin: '0',
    paddingLeft: (padding/24)+'px',
    textAlign: 'left',
    fontWeight: '500',
    fontSize: (fontSize)+'px',
    color: 'rgb(202, 224, 213)',
  };
};

const dice = (height, marginLeft, marginBottom) => {
  return {
    width: height+'px',
    height: height+'px',
    marginLeft: marginLeft+'px',
    marginBottom: marginBottom+'px',
    backgroundColor: 'white',
  };
};

const refresh = (height, marginLeft, marginBottom) => {
  return {
    width: (height-8)+'px',
    height: (height-8)+'px',
    marginLeft: (marginLeft*2)+'px',
    marginTop: '4px',
  };
};

export const styles = {
  sentence,
  dice,
  refresh
};