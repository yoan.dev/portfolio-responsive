import React, { Component } from 'react';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-scroll';

import {styles} from './styles';


class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHover: false,
    };
  }
  _onMouseEnter = () => {
    this.setState({isHover: true});
  }
  _onMouseLeave = () => {
    this.setState({isHover: false});
  }
  render() {
    const {name, height, width, src, delay, onClick, isSmall, focus} = this.props;
    const {isHover} = this.state;
    let sizeImg;
    const zIndex = isHover ? 2 : 0;
    const imgScale = isHover ? 'scale(1.1)' : 'none';
    let fontSize, contain, boxShadow;
    boxShadow = width > height ? '8px 8px 8px black' : '4px 4px 4px black';
    if (width >= 1200) fontSize = 16;
    if (width >= 768 && width < 1200) fontSize = 14;
    if (width >= 576 && width < 768) fontSize = 12;
    if (width < 576) fontSize = 10;
    const imgOpacity = focus === name ? '1' : '0.2';
    if (!isSmall) {
      sizeImg = height > Math.floor(width*1.2) ? (height/4.5) : (width/4);
      const sizeImgMin = sizeImg < 150 ? 150 : sizeImg;
      contain = (
        <Link
          activeClass="active"
          to='projets'
          spy={true}
          smooth={true}
          offset={0}
          duration= {500}
          >
            <Col xs={0} 
              onMouseEnter={this._onMouseEnter} 
              onMouseLeave={this._onMouseLeave} 
              style={{overflow: 'hidden', boxShadow: boxShadow}}  
              className="position-relative"
              data-aos="zoom-in"
              data-aos-delay={delay}
              data-aos-duration="900"
              data-aos-once="true"
              data-aos-easing="ease-in-out-back"
              onClick={event => onClick(event)}
              >
              <div style={styles.textTop(sizeImgMin, fontSize, zIndex)} className="d-flex justify-content-center align-items-center ">
                {data[name].txtTop}
              </div>
              <img src={src} alt={name} style={styles.project(sizeImg, imgScale)} />
              <div style={styles.textBottom(sizeImgMin, fontSize, zIndex)} className="d-flex justify-content-center align-items-center ">
                {data[name].txtBottom}
              </div>
            </Col>
          </Link>
      )
    } else {
      sizeImg = height > Math.floor(width*1.2) ? (height/10) : (width/10);
      const sizeImgMin = sizeImg < 50 ? 50 : sizeImg;
      contain = (
        <Link
          activeClass="active"
          to='projets'
          spy={true}
          smooth={true}
          offset={0}
          duration= {500}
          >
            <div
              className="project"  
              data-aos="fade-up"
              data-aos-delay={delay}
              data-aos-duration="900"
              data-aos-offset="-100"
              data-aos-once="true"
              data-aos-easing="ease-in-out-back">
              <Col xs={0}
                style={{overflow: 'hidden', boxShadow: '4px 4px 4px black',opacity: imgOpacity}}  
                className="position-relative"
                onClick={event => onClick(event)}
              >
                <img src={src} alt={name} style={styles.projectSmall(sizeImgMin)} />
              </Col>
            </div>
        </Link>
      
      )
    }
    return (
      <div>{contain}</div>
  );
  }
}

const data = {
  'poker': {
    txtTop: 'Analyse Poker - 2015',
    txtBottom: 'Langage C',
  },
  'pico': {
    txtTop: 'PICO - Jeu de dès - 2019',
    txtBottom: 'Javascript - React & Redux',
  },
  'open': {
    txtTop: 'Dev. Frontend / Python',
    txtBottom: 'Formation - 2020',
  },
  
}

export default Project;