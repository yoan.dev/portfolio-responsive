

const project = (width, scale) => {
  return {
    position: 'relative',
    zIndex: 1,
    transform: scale,
    transition: 'transform 0.3s',
    backgroundSize: 'cover',
    width: width+'px',
    height: width+'px',
    minWidth: '150px',
    minHeight: '150px',
  }
};
const projectSmall = (width) => {
  return {
    position: 'relative',
    zIndex: 1,
    backgroundSize: 'cover',
    width: width+'px',
    height: width+'px',
    minWidth: '50px',
    minHeight: '50px',
  }
};
const textTop = (width, fontSize, zIndex) => {
  return {
    
    position: 'absolute',
    width: (width)+'px',
    minWidth: '150px',
    zIndex: zIndex,
    height: Math.round(width*0.15)+'px',
    fontSize: fontSize+'px',
    fontWeight: '700',
    color: 'rgb(202, 224, 213)',
    backgroundColor: 'rgba(18, 21, 43, 0.9)',
  }
};
const textBottom = (width, fontSize, zIndex) => {
  return {
    
    position: 'absolute',
    width: (width)+'px',
    minWidth: '150px',
    zIndex: zIndex,
    top: Math.round(width*0.85)+1+'px',
    height: Math.round(width*0.15)+'px',
    fontWeight: '700',
    fontSize: fontSize+'px',
    color: 'rgb(202, 224, 213)',
    backgroundColor: 'rgba(18, 21, 43, 0.9)',
  }
};

export const styles = {
  project,
  textTop,
  textBottom,
  projectSmall,
};