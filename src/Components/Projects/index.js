import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import imgOpen from '../../images/openclassroom.png';
import imgPico from '../../images/dices.jpg';
import imgPoker from '../../images/poker.jpg';
import ContainerProject from './ContainerProject';
import Projet from './Projet';
import {styles} from './styles';

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProjectFocus: null,
    }
  }
  _onClickProject = event => {
    const eventProject = event.target.alt;
    this.setState({isProjectFocus: eventProject});
  }
  render() {
    const {isProjectFocus} = this.state;
    const {width, height} = this.props;
    let classNamesBS = "text-center font-weight-bold";
    if (width < 576) classNamesBS = classNamesBS.substr(0,28) +' h6';
    if (width >= 576) classNamesBS = classNamesBS.substr(0,28) +' h5';
    if (width >= 768) classNamesBS = classNamesBS.substr(0,28) +' h4';
    if (width >= 992) classNamesBS = classNamesBS.substr(0,28) +' h3';
    if (width >= 1200) classNamesBS = classNamesBS.substr(0,28) +' h2';
    const divise = height > Math.floor(width*1.2) ? 18 : 12;
    const CnDirProjet = height > Math.floor(width*1.2) ?
    'd-flex flex-column justify-content-around align-items-center' :
    'd-flex justify-content-around align-items-center';
  
    return (
    <Row style={styles.projects} 
    className="h-100" id="projets">
      {isProjectFocus === null ? (
        <Col xs={12} className="d-flex flex-column justify-content-around" > 
        <Row > {/*La ligne du titre*/}
          <Col xs={12} style={styles.skillsTitle(height,divise)} className={classNamesBS}>
            Projets
          </Col>
        </Row>
        <Row className={CnDirProjet}
        style={{height: 0.8*height+'px'}}>
          {['poker', 'pico', 'open'].map((projet,index) =>(
            <Projet 
              name={projet} 
              key={projet} 
              width={width} 
              height={height}
              src={data[projet].src}
              delay={(index)*100}
              onClick={this._onClickProject}
              isSmall={false}
              />
          ))}
        </Row>
      </Col>) : (
        <ContainerProject name={isProjectFocus} width={width} height={height} onClick={this._onClickProject} />
      )}
    </Row>
  );
  }
}

const data = {
  'poker': {
    src: imgPoker,
  },
  'pico': {
    src: imgPico,
  },
  'open': {
    src: imgOpen,
  },
};

export default Projects;