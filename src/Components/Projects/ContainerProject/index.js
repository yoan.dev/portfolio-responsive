import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import imgOpen from '../../../images/openclassroom.png';
import imgPico from '../../../images/dices.jpg';
import imgPoker from '../../../images/poker.jpg';

import ContainText from '../ContainText';
import Project from '../Projet';
import {styles} from './styles';
import './ContainerProject.css';

class ContainerProject extends Component {
  render() {
    const {width, height, onClick, name} = this.props;
    return (
    <Col xs={12} className="mt-4 d-flex justify-content-center align-items-center" >
      { width > height ? (
        <Row className="w-100 d-flex justify-content-center">
          <Col xs={2} style={styles.containerSmallProject(height)}>
            <Row className="h-100 d-flex flex-column justify-content-around align-items-center">
              {['poker', 'pico', 'open'].map((project, index) => (
                <Project 
                  isSmall={true}
                  name={project}
                  focus={name}
                  delay={index*100}
                  key={project} 
                  width={width} 
                  height={height} 
                  src={data[project].src} 
                  onClick={onClick}
                />
              ))}
            </Row>
          </Col>
          <Col xs={width >= 1200 ? 7 : 9} style={styles.containProject(height)} className="contain__project">
            <ContainText name={name} width={width} height={height} />
          </Col>
        </Row>) : (
        <Row className="w-100 d-flex justify-content-center">
          <Col xs={11} style={styles.containProjectPhone(height)} className="contain__project__phone">
            <ContainText name={name} width={width} height={height} />
          </Col>
          <Col xs={11} style={styles.containerSmallProjectPhone(height)}>
            <Row className="h-100 d-flex justify-content-around align-items-center">
              {['poker', 'pico', 'open'].map((project, index) => (
                <Project 
                  isSmall={true}
                  name={project}
                  focus={name}
                  delay={index*150}
                  key={project} 
                  width={width} 
                  height={height} 
                  src={data[project].src} 
                  onClick={onClick}
                />
              ))}
            </Row>
          </Col>
        </Row>
      )}
    </Col>
  );
  }
}

const data = {
  'poker': {
    src: imgPoker,
  },
  'pico': {
    src: imgPico,
  },
  'open': {
    src: imgOpen,
  },
};

export default ContainerProject;