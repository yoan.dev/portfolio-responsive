const containProject = height => {
  return {
    height: 'auto',
    backgroundColor: '#12152B',
    borderRadius: '15px',
    padding: '0px',
  };
};

const containerSmallProject = height => {
  return {
    height: (0.7*height)+'px',
  };
};
const containProjectPhone = height => {
  return {
    height: 'auto',
    backgroundColor: '#12152B',
    borderRadius: '15px',
    padding: '0px',
  };
};

const containerSmallProjectPhone = height => {
  return {
    height: (0.15*height)+'px',
  };
};

export const styles = {
  containProject,
  containerSmallProject,
  containerSmallProjectPhone,
  containProjectPhone,
};