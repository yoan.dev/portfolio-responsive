import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import imgGitlab from '../../../images/gitlab.png';
import imgDice from '../../../images/linkDice.png';
import imgOpen from '../../../images/linkOpen.jpg';
import Dices from '../Dices';
import {styles} from './styles';

class ContainText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alea: Math.floor(Math.random() * Math.floor(20)) + 1,
    };
  }
  _onClickRefresh = () => {
    this.setState({alea: Math.floor(Math.random() * Math.floor(20)) + 1});
  }
  render() {
    const {alea} = this.state;
    const {name, width, height} = this.props;
    let fontSize, fontSizePoint, pointSize, pointMargeTop, marginLeft, imgHeight;
    if (width < height) {
      if (width >= 300 && width <= 375) {
        if (height <= 569) {fontSize =9; fontSizePoint = 9; pointSize = 5;  pointMargeTop = 4; marginLeft = width/22; imgHeight = 32;}
        if(height > 569 && height <= 667) {fontSize = 10; fontSizePoint = 10; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 32;}
        if (height > 668) {fontSize = 12; fontSizePoint = 11; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 36;}
      } else if (width > 375 && width <= 411) {
        fontSize = 12; fontSizePoint = 12; pointSize = 7;  pointMargeTop = 7; marginLeft = width/22; imgHeight = 36;
      } else if (width > 411 && width < 600) {
        fontSize = 12; fontSizePoint = 12; pointSize = 7;  pointMargeTop = 7; marginLeft = width/22; imgHeight = 36;
      } else if (width >= 600 && width < 801) {
        if (height <= 1024) {fontSize = 16; fontSizePoint = 16; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
        if (height > 1024 && height <= 1280) {fontSize = 18; fontSizePoint = 18; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 52;}
      } else if (width >800 && width <= 1441) {
        if (height <= 1100){fontSizePoint = 16; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
        if (height > 1100 && width <= 1366) {fontSize = 22; fontSizePoint = 22; pointSize = 16; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
      }
    } else {
      if (width >= 300 && width <= 667) {
        if (height <= 320) {fontSize = 7; fontSizePoint = 7; pointSize = 3; pointMargeTop = 4; marginLeft = width/22; imgHeight = 32;}
        if (height > 320 && height <= 360) {fontSize = 9; fontSizePoint = 8; pointSize = 4; pointMargeTop = 4; marginLeft = width/22; imgHeight = 32;}
        if (height > 360 && height <= 375) {fontSize = 9; fontSizePoint =9; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 32;}
      } else if (width > 667 && width <= 736) {
          fontSize = 10; fontSizePoint = 10; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 32;
      } else if (width > 736 && width <= 823) {
          fontSize = 9; fontSizePoint = 9; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 32;
      } else if (width > 823 && width <= 1280) {
        if (height <= 600) {fontSize = 12; fontSizePoint = 12; pointSize = 7; pointMargeTop = 7; marginLeft = width/12; imgHeight = 48;}
        if (height > 600 && height <= 768) {fontSize = 16; fontSizePoint = 16; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
        if (height > 768 && height <= 800) {fontSize = 16; fontSizePoint = 16; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
        if (height > 800) {fontSize = 16; fontSizePoint = 16; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
      } else if (width > 1280 && width <= 1440) {
        if (height <= 600) {fontSize = 12; fontSizePoint = 12; pointSize = 7; pointMargeTop = 7; marginLeft = width/12; imgHeight = 48;}
        if (height > 600 && height <= 800) {fontSize = 14; fontSizePoint = 14; pointSize = 8; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
        if (height > 800) {fontSize = 20; fontSizePoint = 20; pointSize = 12; pointMargeTop = 12; marginLeft = width/12; imgHeight = 48;}
      } else if (width > 1440) {
        fontSize = 18; fontSizePoint = 18; pointSize = 10; pointMargeTop = 10; marginLeft = width/12; imgHeight = 48;
      }
    }
    imgHeight = name === 'poker' ? imgHeight : Math.round(imgHeight*0.5);
    // if (height > 576) {
    //   if (width >= 1200) {fontSize = 16; pointSize = 10; pointMargeTop = 8; marginLeft = width/12; imgHeight = 48;}
    //   if (width >= 768 && width < 1200) {fontSize = 14; pointSize = 9; pointMargeTop = 8; marginLeft = width/12; imgHeight = 40;}
    //   if (width >= 576 && width < 768) {fontSize = 12; pointSize = 7; pointMargeTop = 7; marginLeft = width/12; imgHeight = 36;}
    //   if (width < 576){fontSize = 10; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 32;}
    // } else {
    //   {fontSize = 9; pointSize = 5; pointMargeTop = 5; marginLeft = width/22; imgHeight = 32;}
    // }
    
    return (
    <Row className="w-100 m-0">
      <Col xs={12} style={styles.title(fontSize)} className=" my-2 my-md-3 my-lg-4 d-flex justify-content-center align-items-center">{data[name].title}</Col>
      <Col xs={12} >
        {data[name].sentence1.map(item => (
          <p style={styles.sentence(fontSize, width)}  key={item}>{item}<br/><br/></p>
        ))}
      </Col>
      {name === 'pico' ? <Dices random={alea} onClick={this._onClickRefresh} width={width} height={height} fontSize={fontSize} /> : null}
      <Col xs={12} className="d-flex flex-wrap justify-content-center" style={{marginLeft: (width/12)+'px'}} >
        {data[name].points1.map(item => (
          <Col xs={6} sm={5}  className=" p-0 d-flex" key={item}>
            <div style={styles.point(pointSize, pointMargeTop)}></div>
            <p style={styles.sentencePoint(fontSizePoint)}>{item}</p>
          </Col>
        ))}
      </Col>
      <Col xs={12} style={styles.sentence(fontSize)}>
        {data[name].sentence2.map(item => (
          <p style={styles.sentence(fontSize, width)} key={item}>{name !== 'open' ? <br/> : null}{item}<br/><br/></p>
        ))}
      </Col>
      <Col xs={12} className="p-0 d-flex flex-wrap" style={{margin: '0 '+marginLeft+'px'}} >
        {data[name].points2.map((item) => (
          <Col xs={10} sm={10} className=" p-0 d-flex" key={item}>
            <div style={styles.point(pointSize, pointMargeTop)}></div>
            <p style={styles.sentencePoint(fontSizePoint)}>{item}</p>
          </Col>
        ))}
      </Col>
      <Col xs={12} style={styles.sentence(fontSize)}>
        {name === 'poker' ? <div><br/></div> : <br/>}
        {data[name].sentence3.map((item, index) => (
          <p style={styles.sentence(fontSize, width)}  key={item}>
            {item}{data[name].lien[index] !== null ? (
              <a href={data[name].lien[index]} target="_blank" rel="noopener noreferrer"><img src={data[name].srcImg} alt="lien gitlab"
               style={{width: imgHeight+'px',maxWidth: '64px',maxHeight: '64px', height: imgHeight+'px', marginTop: '-6px',
              marginLeft: name === 'poker' ? '0' : '5px'}} /></a>
            ) : ''}
            </p>
        ))}{<br/>}
      </Col>
    </Row>
  );
  }
}

const data = {
  'poker': {
    title: 'Poker - Programme d\'analyse',
    sentence1: [
      "Conçu en 2015, j'ai spécialement appris les bases du langage C pour développer cet algorithme qui m'a aidé à appronfondir ma connaissance du jeu.",
      "Il analyse le déroulement d'une main au poker à travers l'action produite par le joueur adversaire et six paramètres: ",
    ],
    points1: [
      'Miser avant les autres.',
      'Suivre une mise.',
      "Relancer une mise.",
      "Suivre une relance.",
      "Eventail de main joué en %.",
      "Carte du flop",
    ],
    sentence2: [
      "Ensuite l'algorithme affiche la force de l'adversaire au flop en répartissant son éventail en plusieurs catégories: ",
    ],
    points2: [
      "Très fort - Double paire, brelan, paire supérieure",
      "Fort - Top paire, gros tirage",
      "Moyen - Seconde paire, faible tirage",
      "Faible - 3ème paire, tirage médiocre, cartes hautes",
      "Médiocre - quatrième paire, une carte haute",
    ],
    sentence3: [
      "L'algorithme répète cette dernière étape en demandant la carte de la Turn.",
      "Vous pouvez retrouver cet algorithme sur mon espace gitlab.",
    ],
    lien: [
      null,
      'https://gitlab.com/yoan.dev/poker',
  ],
    srcImg: imgGitlab,

  },
  'pico': {
    title: 'PICO - Jeu de dès',
    sentence1: [
      "PICO est un prototype frontend d'application conçu courant 2019 avec React & Redux dans le but de mettre en pratique mes connaissances théoriques.",
      "Le but du jeu est de trouver un nombre en utilisant TOUS les dès mis à disposition et en se servant de trois opérations, l'addition, la soustraction et la multiplication.",
    ],
    points1: [
    ],
    sentence2: [
      "L'application est conçu à travers cinq sections principales: ",
    ],
    points2: [
      "Solo - Mode facile, moyen et difficile proposant 90 niveaux partant de 3 à 5 dès.",
      "Défis - Trouver le plus de nombres avec timer et résoudre des puzzles en 4 à 6 dès.",
      "Duel - Défier d'autres joueurs en 1v1 ainsi qu'un mode survie en 3v3, 5v5 et 10v10.",
      "Réussite - Titre et haut-fait à accomplir durant la progression du jeu.",
      "Magasin - Design de dès à acheter avec des points de réussite."
    ],
    sentence3: [
      "Voici le lien du prototype.",
    ],
    lien: ['https://serene-reaches-17477.herokuapp.com/home/sectionSolo'],
    srcImg: imgDice,
  },
  'open': {
    title: 'Formation - OpenClassrooms',
    sentence1: [
      "Grâce à OpenClassrooms, j'ai pu débuter dans la programmation à travers plusieurs langages comme C, HTML, CSS, JS et Git.",
      "Depuis Novembre 2019, j'ai commencé une formation avec OpenClassrooms pour devenir Développeur. Je suis actuellement en recherche d'une entreprise pour réaliser ma formation en alternance.",
    ],
    points1: [
    ],
    sentence2: [
      "Je propose deux parcours qui me tiennent à coeur aux entreprises. Un orienté Frontend et l'autre Backend. Quelques points sur la formation: ",
    ],
    points2: [
      "Analyser un cahier des charges et choisir une solution adaptée parmi celles existantes.",
      "Concevoir l'architecture technique d'une application à l'aide de diagrammes UML.",
      "Communiquer avec une base de données pour stocker et requêter des informations.",
      "Mettre en oeuvre des tests unitaires et fonctionnels.",
      "Produire une documentation technique et fonctionnelle de l'application."
    ],
    sentence3: [
      "Formation - Développeur Frontend - ",
      "Formation - Développeur Python/Django - "
    ],
    lien: [
      'https://openclassrooms.com/fr/paths/60-developpeur-dapplication-frontend',
      "https://openclassrooms.com/fr/paths/68-developpeur-dapplication-python",
  ],
    srcImg: imgOpen,
  },

}

export default ContainText;