const title = fontSize => {
  return {
    fontWeight: '700',
    fontSize: (fontSize+4)+'px',
    color: 'rgb(202, 224, 213)',
  };
};

const sentence = (fontSize, padding) => {
  return {
    margin: '0',
    padding: '0 '+(padding/24)+'px',
    textAlign: 'left',
    fontWeight: '500',
    fontSize: (fontSize)+'px',
    color: 'rgb(202, 224, 213)',
  };
};
const sentencePoint = fontSize => {
  return {
    margin: '0',
    textAlign: 'left',
    fontWeight: '500',
    fontSize: (fontSize)+'px',
    color: 'rgb(202, 224, 213)',
  };
};

const point = (size, marginTop) => {
  return {
    width: size+'px',
    height: size+'px',
    marginTop: marginTop+'px',
    marginRight: '5px',
    borderRadius: '50%',
    backgroundColor: 'rgb(133, 224, 209)',
    userSelect: 'none',
  };
};

export const styles = {
  title,
  sentence,
  point,
  sentencePoint,
};