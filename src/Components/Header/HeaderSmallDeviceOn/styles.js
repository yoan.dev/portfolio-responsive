const HeaderSmallDeviceOnBtn = {
    backgroundColor: '#12152B',
};
const barOne = {
    margin: '4px 0',
    height: '3px',
    borderRadius: '4px',
    backgroundColor: 'rgb(202, 224, 213)',
    transform: 'rotate(45deg) translate(4px, 4px)',
};
const barTwo = {
    margin: '2px 0',
    height: '3px',
    borderRadius: '4px',
    backgroundColor: 'rgb(202, 224, 213)',
    transform: 'rotate(-45deg) translate(2px, -3px)',
};

export const styles = {
    HeaderSmallDeviceOnBtn,
    barOne,
    barTwo,
}
