import React, { Component } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import HeaderButton from '../../Button/HeaderButton';
import {styles} from './styles';


class HeaderSmallDeviceOn extends Component {
  render() {
    const {onClick, width} = this.props;
    let fontSize;
    if (width >= 1200) fontSize = 16;
    if (width >= 992 && width < 1200) fontSize = 16;
    if (width >= 768 && width < 992) fontSize = 14;
    if (width >= 576 && width < 768) fontSize = 12;
    if (width < 576) fontSize = 10;
    return (
      <Col xs={4} style={styles.HeaderSmallDeviceOnBtn}  >
        <Row className="d-flex justify-content-end mt-3">
          <Col xs={3} onClick={onClick} >
            <Row style={{position: 'relative', right: '10px', top: '-10px'}}>
              <Col xs={6} style={styles.barOne}></Col>
              <Col xs={6} style={styles.barTwo}></Col>
            </Row>
          </Col>
        </Row>
        <Row className="d-flex flex-column align-items-center">
          {['accueil', 'présentation', 'compétences', 'projets', 'contact'].map(btn => (
            <HeaderButton 
              isSmallHeader={true} 
              name={btn} 
              key={btn} 
              xs={10} 
              fontSize={fontSize} 
              onClick={onClick}
            />
        ))}
        </Row>
      </Col>
  );
  }
}

export default HeaderSmallDeviceOn;