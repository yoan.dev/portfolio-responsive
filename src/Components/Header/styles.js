const header = height => {
    return {
    width: '100%',
    height: Math.round(height)+'px',
    backgroundColor: '#12152B',
    position: 'fixed',
    zIndex: '3',
    };
    
};
const headerSmallDevice = height => {
    return {
      width: '100%',
      height: Math.round(height)+'px',
      backgroundColor: '',
      position: 'fixed',
      zIndex: '3',
    }
};

export const styles = {
    header,
    headerSmallDevice,
};