import React, { Component } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {styles} from './styles';


class HeaderSmallDeviceOff extends Component {
  render() {
    const {onClick} = this.props;
    return (
      <Col xs={1} style={styles.HeaderSmallDeviceOffBtn} onClick={() => onClick()}>
        <Row className="mr-1 d-flex justify-content-center">
            <Col xs={10} style={styles.bar}></Col>
            <Col xs={10} style={styles.bar}></Col>
            <Col xs={10} style={styles.bar}></Col>
        </Row>
      </Col>
  );
  }
}

export default HeaderSmallDeviceOff;