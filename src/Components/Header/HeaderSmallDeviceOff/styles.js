const HeaderSmallDeviceOffBtn = {
    margin: '5px 0 2px 0',
    maxWidth: '50px',
    minWidth: '30px',
    backgroundColor: '#12152B',
};
const bar = {
    margin: '3px 0',
    height: '2px',
    borderRadius: '3px',
    backgroundColor: 'rgb(202, 224, 213)',
};


export const styles = {
    HeaderSmallDeviceOffBtn,
    bar,
}
