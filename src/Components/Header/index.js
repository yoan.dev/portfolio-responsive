import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import HeaderSmallDeviceOff from './HeaderSmallDeviceOff';
import HeaderSmallDeviceOn from './HeaderSmallDeviceOn';
import HeaderButton from '../Button/HeaderButton';

import {styles} from './styles';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSmallHeaderOn: false,
    };
  }
  _onClickSmallHeader = () => {
    const {isSmallHeaderOn} = this.state;
    this.setState({isSmallHeaderOn: !isSmallHeaderOn});
  }
  render() {
    const {isSmallHeaderOn} = this.state;
    const {width} = this.props;
    let headerHeight;
    if (width >= 1200) headerHeight = 48;
    if (width >= 992 && width < 1200) headerHeight = 44;
    if (width >= 768 && width < 992) headerHeight = 40;
    if (width >= 576 && width < 768) headerHeight = 36;
    if (width < 576) headerHeight = 32;
    let fontSize;
    if (width >= 1200) fontSize = 16;
    if (width >= 992 && width < 1200) fontSize = 16;
    if (width >= 768 && width < 992) fontSize = 14;
    if (width >= 576 && width < 768) fontSize = 12;
    if (width < 576) fontSize = 10;
    return (
      <Row 
        className="d-flex justify-content-xl-end justify-content-sm-center justify-content-end"
        style={isSmallHeaderOn ? styles.headerSmallDevice(headerHeight) : styles.header(headerHeight)}
      >
        {(
          (width < 576) && (
            isSmallHeaderOn ? 
            <HeaderSmallDeviceOn onClick={this._onClickSmallHeader} width={width} />
            : <HeaderSmallDeviceOff onClick={this._onClickSmallHeader} width={width} />
          )) ||
          (( width >= 1200 &&  width < 1388) && (
          ['accueil', 'présen.', 'compét.', 'projets', 'contact'].map(btn => (
            <HeaderButton 
              isSmallHeader={false} 
              name={btn} 
              key={btn} 
              xs={3} 
              width={width} 
              fontSize={fontSize}
            />
          )))) || (
          ['accueil', 'présentation', 'compétences', 'projets', 'contact'].map(btn => (
            <HeaderButton 
              isSmallHeader={false} 
              name={btn} 
              key={btn} 
              xs={3} 
              width={width} 
              fontSize={fontSize}
            />
        )))}
      </Row>
  );
  }
}

export default Header;