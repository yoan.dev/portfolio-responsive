import styled, {keyframes} from 'styled-components';

const skillAnim = width => {
  const frameSkillAnim = keyframes`
  0% {
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
      width: 0px;
  }
  100% {
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
      width: ${width};
  }`;
  const anim = styled.div`
    width: 0px;
    background-color: rgb(9, 196, 9);
    box-shadow: 0 3px 0 rgb(16, 240, 16) inset,
      0 4px 0 rgba(16, 240, 16, 1) inset,
      0 5px 0 rgba(16, 240, 16, 1) inset,
      0 6px 0 rgba(16, 240, 16, 0.8) inset,
      0 7px 0 rgba(14, 240, 16, 0.6) inset,
      0 8px 0 rgba(15, 240, 16, 0.4) inset,
      0 9px 0 rgba(15, 240, 16, 0.2) inset;
  
  animation: ${frameSkillAnim} 2s both;
  `;
  return anim;
}

const textInBar = (positionLeft, fontSize) => {
  return {
    position: 'absolute',
    left: positionLeft,
    textShadow: '2px 1px 1px black',
    color: 'white',
    fontSize: fontSize,
    fontWeight: 'bold',
    margin: '0',
  };
};

const BarSkillContainer = {
  backgroundColor: 'rgba(43, 34, 34,0.95)',
  borderRadius: '4px',
  border: 'solid 1px black',
};
const barEndAnim = (height, width) => {
  return {
    height: height,
    width: width,
    backgroundColor: 'rgb(9, 196, 9)',
    boxShadow: `0 3px 0 rgb(16, 240, 16) inset,0 4px 0 rgba(16, 240, 16, 1) inset,
      0 5px 0 rgba(16, 240, 16, 1) inset,
      0 6px 0 rgba(16, 240, 16, 0.8) inset,
      0 7px 0 rgba(14, 240, 16, 0.6) inset,
      0 8px 0 rgba(15, 240, 16, 0.4) inset,
      0 9px 0 rgba(15, 240, 16, 0.2) inset`,
  }
}


export const styles = {
  skillAnim,
  textInBar,
  BarSkillContainer,
  barEndAnim,
};