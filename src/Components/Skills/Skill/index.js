import React, { Component } from 'react';
import ScrollTrigger from 'react-scroll-trigger';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Media from 'react-bootstrap/Media'
import Counter from '../Counter';

import imgCss from '../../../images/css.png';
import imgHtml from '../../../images/html.png';
import imgJs from '../../../images/js.png';
import imgReact from '../../../images/react.png';
import imgPy from '../../../images/imgPy.png';
import imgGit from '../../../images/git.png';
import imgAnglais from '../../../images/anglais.png';

import {styles} from './styles';

class Skills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: null,
      count: 0,
    };
  }
  onEnterViewport = () => {
    const count = this.state.count;
      this.setState({
      visible: true,
      count: count + 1,
    });
    }
    
  onLeaveViewport = () => {
    const count = this.state.count;
    this.setState({
      visible: false,
      count: count + 1,
    });
  }

  render() {
    const {visible, count} = this.state;
    const {img, value, width} = this.props;
    const BarSkill = styles.skillAnim(data[value].widthAnim);
    const heightSkill = width >= 992 ? '24px' : '18px';
    const fontSizeSkill = width >= 992 ? '16px' : '12px';
    
    return (
        <Media className="my-2 my-lg-4 " style={{height: heightSkill}}>
          {img ? <img width={24} height={24} alt="logo" style={data[value].src === imgGit ? {transform:'rotate(45deg) scale(1.15)'} : {}}
          src={data[value].src} className="mr-1 mt-1" />
              : ''} 
          <Media.Body>
            <ScrollTrigger onEnter={this.onEnterViewport} onExit={this.onLeaveViewport} throttleScroll={0}>
              <Col xs={12} className="mt-1" style={{borderRadius: '4px'}} >
              
                <Row className='bg-dark position-relative' style={styles.BarSkillContainer}>
                  
                   {count < 4 && <BarSkill>
                    <Col xs={0} style={{height: heightSkill}}>
                    
                    </Col>
                  </BarSkill>}
                  { count >= 4 && <Col xs={0} style={styles.barEndAnim(heightSkill,data[value].widthAnim)}>
                    
                    </Col>}
                  {(visible && data[value].step) ? (count < 4 ? <Counter max={parseInt(data[value].widthAnim.substr(0,2))}
                   step={ data[value].step} fontSize={fontSizeSkill} /> : (
                    <Col xs={0} style={styles.textInBar('45%', fontSizeSkill)}>{data[value].widthAnim}</Col>
                   ))
                  : <Col xs={0} style={styles.textInBar('39%', fontSizeSkill)}>{data[value].name}</Col>}
                </Row>
              </Col>
            </ScrollTrigger>
              
          </Media.Body>
        </Media>
  );
  }
}
const data = {
  0: {
    src: imgCss,
    widthAnim: '68%',
    step: 25,
  },
  1: {
    src: imgHtml,
    widthAnim: '82%',
    step: 21,
  },
  2: {
    src: imgJs,
    widthAnim: '55%',
    step: 33,
  },
  3: {
    src: imgReact,
    widthAnim: '73%',
    step: 23,
  },
   4: {
    src: imgPy,
    widthAnim: '67%',
    step: 23,
  },
  5: {
    src: imgGit,
    widthAnim: '53%',
    step: 33,
  },
  6: {
    src: imgAnglais,
    widthAnim: '63%',
    step: 23,
  },

  7: {
    src: '',
    widthAnim: '87%',
    name: 'Curieux',
  },
  8: {
    src: '',
    widthAnim: '71%',
    name: 'Créatif',
  },
  9: {
    src: '',
    widthAnim: '81%',
    name: 'Autonome',
  },
  10: {
    src: '',
    widthAnim: '67%',
    name: 'Rigoureux',
  },
  11: {
    src: '',
    widthAnim: '75%',
    name: 'Coopératif',
  },
  12: {
    src: '',
    widthAnim: '91%',
    name: 'Passionné',
  },
  13: {
    src: '',
    widthAnim: '77%',
    name: 'Flexible',
  },
};

export default Skills;