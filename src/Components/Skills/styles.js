import imgMatrix from '../../images/matrix1.jpg';

const skills = {
  background: `url(${imgMatrix}) fixed no-repeat`,
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};
const skillsTitle = {
  color: 'rgb(202, 224, 213)',
}

export const styles = {
    skills,
    skillsTitle,
};