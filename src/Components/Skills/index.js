import React, { Component } from 'react';
import Skill from './Skill';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import imgMan from '../../images/personnage.png';
import {styles} from './styles';

class Skills extends Component {
  render() {
    const {width, height} = this.props;
    let classNamesBS = "text-center font-weight-bold";
    if (width < 576) classNamesBS = classNamesBS.substr(0,28) +' h6';
    if (width >= 576) classNamesBS = classNamesBS.substr(0,28) +' h5';
    if (width >= 768) classNamesBS = classNamesBS.substr(0,28) +' h4';
    if (width >= 992) classNamesBS = classNamesBS.substr(0,28) +' h3';
    if (width >= 1200) classNamesBS = classNamesBS.substr(0,28) +' h2';
    
    return (
    <Row style={styles.skills} className="h-100" id="compétences">
      <Col xs={12} className="d-flex flex-column justify-content-center" >
        <Row > {/*La ligne du titre*/}
          <Col xs={12} style={styles.skillsTitle} className={classNamesBS}>Compétences</Col>
        </Row>
        { (width*1.2) > height ? (<Row className=" pt-3 d-flex justify-content-center
         align-items-center" style={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}} > {/*La lignes des deux blocs skills et image*/}
          <Col xs={4} lg={4} xl={3}>
            {[0,1,2,3,4,5,6].map(item => (
              <Skill img={true} value={item} key={item} width={width} height={height} />
            ))}
          </Col>
          <Col xs={2} className="d-flex justify-content-center">
            <img src={imgMan} alt="imgMan" style={{width: (width*0.08)+'px'}}  />
          </Col>
          <Col xs={4} lg={4} xl={3}>
          {[7,8,9,10,11,12,13].map(item => (
              <Skill img={false} value={item} key={item} width={width} height={height} />
            ))}
          </Col>
        </Row>) : (
        <Row className="d-flex justify-content-center align-items-center">
          <Col xs={11}>
            <Row className="w-100 d-flex align-items-center justify-content-center" style={{height: (height*0.8)+'px'}}>
              <Col xs={9} className="">
                {[0,1,2,3,4,5,6].map(item => (
                  <Skill img={true} value={item} key={item} width={width} height={height} />
                ))}
              </Col>
              <Col xs={9} className="mt-3">
                {[7,8,9,10,11,12,13].map(item => (
                  <Skill img={false} value={item} key={item} width={width} height={height} />
                ))}
              </Col>
            </Row>
          </Col>
        </Row>
        )}
      </Col>
    </Row>
  );
  }
}

export default Skills;

