import React, {useState, useEffect, useRef} from 'react';
import Col from 'react-bootstrap/Col';
import {styles} from '../Skill/styles';

function Counter({max, step, fontSize}) {
    const [count, setCount] = useState(0);
    
    useInterval(() => {
      // Your custom logic here
      if (count !== max) {
        setCount(count + 1);
      }
      
    }, step);
  return <Col xs={0} style={styles.textInBar('45%', fontSize)}>{count+'%'}</Col>;
  }
  
  function useInterval(callback, delay) {
    const savedCallback = useRef();
  
    // Remember the latest function.
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);
  
    // Set up the interval.
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  }

  export default Counter;