import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';

import Header from '../Header';
import Landpage from '../Landpage';
import Presentation from '../Presentation';
import Skills from '../Skills';
import Projects from '../Projects';
import Contact from '../Contact';
import Footer from '../Footer';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
      height: 0,
    };
  }
  componentDidMount = () => {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }
  render() {
    const {width, height} = this.state;
    return (
    <Container fluid style={{ height: height}}>
      <Header width={width} height={height} />
      <Landpage width={width} height={height} />
      <Presentation width={width} height={height} />
      <Skills width={width} height={height} />
      <Projects width={width} height={height} />
      <Contact width={width} height={height} />
      <Footer width={width} height={height} />
    </Container>
  );
  }
}


export default App;
