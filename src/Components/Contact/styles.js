import imgGeo from '../../images/geometry2.jpg'

const contact = {
  background: `url(${imgGeo}) fixed no-repeat`,
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};

const title = fontSize => {
  return {
    fontSize: fontSize+'px',
    color: 'rgb(202, 224, 213)',
    fontWeight: '700',
  }
}
const sentence = fontSize => {
  return {
    fontSize: fontSize+'px',
    color: 'rgb(202, 224, 213)',
    fontWeight: '700',
  }
}

const button = fontSize => {
  return {
    borderRadius: '6px',
    fontSize: fontSize,
    backgroundColor: '#237175',
    border:'none',
    fontWeight: '700',
    color: 'rgb(202, 224, 213)',
  }
}

export const styles = {
    contact,
    title,
    button,
    sentence,
};