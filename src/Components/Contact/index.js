import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import imgMan from '../../images/personnage2.png';
import {styles} from './styles';

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'',
      mail: '',
      message:'',
    }
  }
  _onChangeName = e => {
    this.setState({name: e.target.value})
  }
  _onChangeEmail = e => {
    this.setState({mail: e.target.value})
  }
  _onChangeMessage = e => {
    this.setState({message: e.target.value})
  }
  render() {
    const {width, height} = this.props;
    let labelDisplay = true, formSize = '', textAreaRow = '4', marginTop: '0px', fontSize = '16px';
    if (width > height) {
      if (height < 500) {labelDisplay = false; formSize = 'sm'; textAreaRow = '2'; marginTop = '30px'; fontSize = '14px';}
    }
    return (
    <Row style={styles.contact} className="h-100 d-flex justify-content-center align-items-center" id="contact">
      
      {width > height && 
      <Col xs={2} className="d-flex justify-content-center align-items-center" 
      style={{height: (height*0.8)+'px',marginTop: marginTop}}>
        <img src={imgMan} alt="imgMan" style={{width: (width*0.08)+'px', maxWidth: '120px', minWidth: '60px'}} />
      </Col>}
      <Col xs={width > height ? 6 : 8} className="d-flex flex-column justify-content-center align-items-center" 
      style={{height: (height*0.8)+'px', marginTop: marginTop}}>
        <div style={styles.sentence(18)} >Rencontrons-nous!</div>
      <Form  className={width < 600 ? 'w-100' : 'w-75'} action="mailto:bousquet_yoan11@yahoo.fr" method="post" encType="text/plain">
        <Form.Group controlId="formBasicName">
          {labelDisplay && <Form.Label style={styles.title(16)}>Nom</Form.Label>}
          <Form.Control size={formSize} type="text" onChange={this._onChangeName} placeholder="Nom" />
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          {labelDisplay && <Form.Label style={styles.title(16)}>mail</Form.Label>}
          <Form.Control size={formSize}  type="email" onChange={this._onChangeEmail} placeholder="Adresse mail" />
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlTextarea1">
          {labelDisplay && <Form.Label style={styles.title(16)}>Message</Form.Label>}
          <Form.Control size={formSize} as="textarea" onChange={this._onChangeMessage} placeholder="Message" style={{maxHeight: '200px'}} rows={textAreaRow} />
        </Form.Group>

        <button style={styles.button(fontSize)} type="submit">
          Envoyer
        </button>
      </Form>
      </Col>
    </Row>
  );
  }
}

export default Contact;